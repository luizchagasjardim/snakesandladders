/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SNLGame.cpp
 * Author: senhormouse
 * 
 * Created on 12 de abril de 2019, 08:10
 */

#include <vector>
#include <time.h>
#include "SNLGame.h"

bool SNLGame::firstGame = true;

SNLGame::SNLGame(Board b) : board(b) {
}

SNLGame::SNLGame(const SNLGame& orig) {
}

SNLGame::~SNLGame() {
}

SNLGame* SNLGame::addPlayer(std::string name) {
    players.push_back(Player(name));
    return this;
}

void SNLGame::start() {
    if (firstGame) {
        firstGame = false;
        srand(time(NULL));
    }
    for (auto& player : players) {
        player.setPosition(1);
    }
}

bool SNLGame::isFinished() {
    return finished;
}

void SNLGame::play() {
    Player& player = players[currentPlayer];
    //move
    player.setPosition(board.move(player.getPosition() + rand() % 6 + 1));
    //check if finished
    if (player.getPosition() == board.getLast()) {
        winner = player.getName();
        finished = true;
        ++turns;
        return;
    }
    ++currentPlayer;
    if (currentPlayer >= players.size()) {
        currentPlayer = 0;
        ++turns;
    }
}

int SNLGame::getTurns() {
    return turns;
}

int SNLGame::getCurrentPlayer() {
    return currentPlayer;
}

int SNLGame::getNumberOfPlayers() {
    return players.size();
}

std::string SNLGame::getWinner() {
    return winner;
}