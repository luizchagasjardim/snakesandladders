/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SNLGame.h
 * Author: senhormouse
 *
 * Created on 12 de abril de 2019, 08:10
 */

#ifndef SNLGAME_H
#define SNLGAME_H

#include <vector>
#include <sstream>
#include "Player.h"
#include "Board.h"

class SNLGame {
public:
    SNLGame(Board);
    SNLGame(const SNLGame& orig);
    virtual ~SNLGame();
    SNLGame* addPlayer(std::string);
    void start();
    bool isFinished();
    void play();
    int getTurns();
    int getCurrentPlayer();
    int getNumberOfPlayers();
    std::string getWinner();

    friend std::ostream& operator<<(std::ostream& out, const SNLGame& game) {
        std::stringstream str;
        str << "end of turn " << game.turns << std::endl;
        for (Player player : game.players) {
            str << "\t" << player.getName() << "@" << player.getPosition() << std::endl;
        }
        return out << str.str();
    }
private:
    std::vector<Player> players;
    int currentPlayer = 0;
    Board board;
    int turns = 0;
    std::string winner = "The game is not finished yet.";
    bool finished = false;
    static bool firstGame;
};



#endif /* SNLGAME_H */

