/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Player.h
 * Author: senhormouse
 *
 * Created on 12 de abril de 2019, 08:29
 */

#ifndef PLAYER_H
#define PLAYER_H

#include <string>

class Player {
public:
    Player(std::string);
    Player(const Player& orig);
    virtual ~Player();
    void setPosition(int);
    int getPosition();
    std::string getName();
private:
    std::string name;
    int position;
};

#endif /* PLAYER_H */

