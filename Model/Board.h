/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Board.h
 * Author: senhormouse
 *
 * Created on 12 de abril de 2019, 08:35
 */

#ifndef BOARD_H
#define BOARD_H

#include <unordered_map>

class Board {
public:
    Board(); //creates empty board
    Board(const Board& orig);
    virtual ~Board();
    int getLast();
    int move(int) const;
    static Board StandardBoard1();
    int getMinimumTurnsToWin() const;
    class BoardMaker {
    public:
        BoardMaker(int);
        BoardMaker(const BoardMaker& orig);
        virtual ~BoardMaker();
        BoardMaker* addSnakeOrLadder(int,int);
        Board build();
    private:
        int lastPosition;
        std::unordered_map<int,int> snakesAndLadders;
    };
private:
    Board(int, std::unordered_map<int,int>);
    int lastPosition;
    std::unordered_map<int, int> snakesAndLadders;
};

#endif /* BOARD_H */