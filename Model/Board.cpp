/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Board.cpp
 * Author: senhormouse
 * 
 * Created on 12 de abril de 2019, 08:35
 */

#include <vector>
#include <set>

#include "Board.h"

Board::Board() {
    
}

Board Board::StandardBoard1() {

    Board b;

    b.lastPosition = 100;
    //snakes
    b.snakesAndLadders[23] = 2;
    b.snakesAndLadders[34] = 15;
    b.snakesAndLadders[52] = 31;
    b.snakesAndLadders[62] = 43;
    b.snakesAndLadders[89] = 68;
    b.snakesAndLadders[95] = 74;
    //ladders
    b.snakesAndLadders[10] = 33;
    b.snakesAndLadders[16] = 37;
    b.snakesAndLadders[21] = 41;
    b.snakesAndLadders[35] = 54;
    b.snakesAndLadders[44] = 76;
    b.snakesAndLadders[80] = 99;

    return b;

}

Board::Board(int pos, std::unordered_map<int,int> snl) : lastPosition(pos), snakesAndLadders(snl) {
    
}

Board::Board(const Board& orig) {
    lastPosition = orig.lastPosition;
    snakesAndLadders = orig.snakesAndLadders;
}

Board::~Board() {
}

int Board::getLast() {
    return lastPosition;
}

int Board::move(int oldPos) const {

    //walk backwards
    int pos = oldPos;

    if (pos > lastPosition) {
        pos = 200 - pos;
    }

    if (snakesAndLadders.find(pos) != snakesAndLadders.end()) {
        pos = snakesAndLadders.at(pos);
    }

    return pos;
}

//can run forever if there's a snake on lastPosition or if there are snakes in 6 consecutive positions
//this could be restricted through the class BoardMaker
int Board::getMinimumTurnsToWin() const {
    int numberOfTurns = 0;
    std::set<int> possiblePositions;
    possiblePositions.insert(1);
    while (possiblePositions.find(lastPosition) == possiblePositions.end()) {
        std::set<int> nextPossiblePositions;
        for (int pos : possiblePositions) {
            for (int i = 1; i <= 6; i++) {
                nextPossiblePositions.insert(move(pos+i));
            }
        }
        possiblePositions = nextPossiblePositions;
        numberOfTurns++;
    }
    return numberOfTurns;
}

Board::BoardMaker::BoardMaker(int pos) : lastPosition(pos) {
}

Board::BoardMaker::BoardMaker(const BoardMaker& orig) {
    lastPosition = orig.lastPosition;
    snakesAndLadders = orig.snakesAndLadders;    
}

Board::BoardMaker::~BoardMaker() {
    
}

Board::BoardMaker* Board::BoardMaker::addSnakeOrLadder(int start, int end) {
    if (snakesAndLadders.find(start) != snakesAndLadders.end()) {
        throw std::runtime_error("position already contains snake or ladder");
    }
    snakesAndLadders[start] = end;
}

Board Board::BoardMaker::build() {
    return Board(lastPosition, snakesAndLadders);
}