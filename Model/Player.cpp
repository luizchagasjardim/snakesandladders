/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Player.cpp
 * Author: senhormouse
 * 
 * Created on 12 de abril de 2019, 08:29
 */

#include "Player.h"
#include "Board.h"

Player::Player(std::string name) : name(name) {
}

Player::Player(const Player& orig) {
    name = orig.name;
    position = orig.position;
}

Player::~Player() {
}

void Player::setPosition(int pos) {
    position = pos;
}

int Player::getPosition() {
    return position;
}

std::string Player::getName() {
    return name;
}