/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: senhormouse
 *
 * Created on 12 de abril de 2019, 08:06
 */

#include <cstdlib>
#include<iostream>
#include "Model/SNLGame.h"
#include "Model/Board.h"

SNLGame playAGame(const Board&, int);

/*
 * 
 */
int main(int argc, char** argv) {

    const Board b = Board::StandardBoard1();
    const int numberOfGames = 1000000;
    const int numberOfPlayers = 2;
    int wins[numberOfPlayers];
    for (int i = 0; i < numberOfPlayers; i++) {
        wins[i] = 0;
    }
    int totalNumberOfTurns = 0;
    int minimumNumberOfTurnsToWin;
    int maximumNumberOfTurnsToWin;

    for (int i = 0; i < numberOfGames; i++) {
        SNLGame game = playAGame(b, numberOfPlayers);
        wins[game.getCurrentPlayer()]++;
        totalNumberOfTurns += game.getTurns();
        if (i == 0) {
            minimumNumberOfTurnsToWin = game.getTurns();
            maximumNumberOfTurnsToWin = game.getTurns();
        } else {
            if (game.getTurns() < minimumNumberOfTurnsToWin) {
                minimumNumberOfTurnsToWin = game.getTurns();
            }
            if (game.getTurns() > maximumNumberOfTurnsToWin) {
                maximumNumberOfTurnsToWin = game.getTurns();
            }
        }
    }

    for (int i = 0; i < numberOfPlayers; i++) {
        std::cout << std::endl;
        std::cout << "p" << i << " won " << wins[i] << " times out of " << numberOfGames << " games with " << numberOfPlayers << " players." << std::endl;
        std::cout << "p" << i << " win percentage: " << ((double) (wins[i]* 100)) / numberOfGames << "%" << std::endl;
    }
    
    std::cout << std::endl;
    std::cout << "minimum number of turns possible for this board: " << b.getMinimumTurnsToWin() << std::endl;
    std::cout << "minimum number of turns in all games: " << minimumNumberOfTurnsToWin << std::endl;
    std::cout << "maximum number of turns in all games: " << maximumNumberOfTurnsToWin << std::endl;
    std::cout << "average number of turns in a game: " << ((double) totalNumberOfTurns)/numberOfGames << std::endl;

    return 0;
}

//plays a game on board b with numberOfPlayers players and returns which player won
SNLGame playAGame(const Board& b, int numberOfPlayers) {

    SNLGame game(b);
    for (int i = 0; i < numberOfPlayers; i++) {
        game.addPlayer("P" + i);
    }
    game.start();
    while (!game.isFinished()) {
        game.play();
//        if (game.getCurrentPlayer() == game.getNumberOfPlayers() - 1) {
//            std::cout << game << std::endl;;
//        }
    }

//    std::cout << "Game finished on turn " << game.getTurns() << ". Winner is " << game.getWinner() << "." << std::endl;

    return game;

}